import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { JobDto } from 'src/app/models/job';
import { RegistrationRequest } from 'src/app/models/reg-request';
import { RoleDto } from 'src/app/models/role';
import { AlertService } from 'src/app/services/alert.service';
import { JobService } from 'src/app/services/job.service';
import { RoleService } from 'src/app/services/role.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  jobs: JobDto[] = [];
  roles: RoleDto[] = [];

  @ViewChild('image') image!: ElementRef<HTMLImageElement>;
  @ViewChild('tooltip') tooltip!: ElementRef<HTMLDivElement>;

  constructor(private userService: UserService,
    private dialogRef: MatDialogRef<RegistrationComponent>,
    private alertService: AlertService,
    private jobService: JobService,
    private roleService: RoleService) {
    this.form = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      job: new FormControl('', Validators.required),
      roles: new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
    this.jobService.findAll().subscribe(response => {
      this.jobs = response;
    })
    this.roleService.findAll().subscribe(response => {
      this.roles = response;
    })
  }

  showTooltip() {
    this.tooltip.nativeElement.style.display = 'flex';
  }

  hideTooltip() {
    this.tooltip.nativeElement.style.display = 'none';
  }

  saveUser() {
    if (this.form.valid) {
      const formValue = this.form.value;

      const body: RegistrationRequest = {
        firstName: formValue.firstName,
        lastName: formValue.lastName,
        email: formValue.email,
        job: formValue.job,
        roles: formValue.roles
      };

      this.userService.createUser(body).subscribe(response => {
        this.dialogRef.close(response);
      }, error => {
        if (typeof error.error === 'object' && 'status' in error.error) {
          this.alertService.showAlert(error.error.status, error.error.message);
        } else {
          alert(`${error.message}`);
        }
        this.dialogRef.close([]);
      })
    }
  }

}
