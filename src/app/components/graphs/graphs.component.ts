import { Component, Input, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import * as moment from 'moment';
import { CategoryService } from 'src/app/services/category.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ChartService } from 'src/app/services/chart.service';
import { FileService } from 'src/app/services/file.service';
import { AlertService } from 'src/app/services/alert.service';
import { ChartDto } from 'src/app/models/chart';
import { concatMap } from 'rxjs';
import { Chart } from 'chart.js/auto';
import plugin from 'src/app/plugins/plugin';
import { Company } from 'src/app/models/company';
import { UserService } from 'src/app/services/user.service';
import { UserDto } from 'src/app/models/user';
import { CompanyService } from 'src/app/services/company.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employee } from 'src/app/models/employee';
import { CalculationRqDto } from 'src/app/models/calculation-rq-dto';
import { CalculationService } from 'src/app/services/calculation.service';
import { CalculationRsDto } from 'src/app/models/calculation-rs-dto';

@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.component.html',
  styleUrls: ['./graphs.component.css']
})
export class GraphsComponent implements OnInit {

  title = 'Графики';
  types: string[] = ['Динамика продаж', 'Средний чек', 'Количество сделок'];
  form: FormGroup;
  chart?: Chart;
  user: UserDto;
  data: CalculationRsDto[] = [];

  categories: Category[] = [];
  companies: Company[] = [];
  employees: Employee[] = [];

  constructor(
    private companyService: CompanyService,
    private categoryService: CategoryService,
    private employeeService: EmployeeService,
    private graphService: CalculationService,
    private chartService: ChartService,
    private fileService: FileService,
    private alertService: AlertService,
    private userService: UserService) {
      this.form = new FormGroup({
        type: new FormControl('', Validators.required),
        companies: new FormControl<Company[]>([]),
        categories: new FormControl<Category[]>([]),
        employees: new FormControl<Employee[]>([]),
        startDate: new FormControl('', Validators.required),
        endDate: new FormControl('', Validators.required)
      });
      this.user = this.userService.getUserDetails()!;
    }

  ngOnInit(): void {
    this.companyService.findAll().subscribe(response => {
      this.companies = response;
    });
    this.categoryService.findAll().subscribe(response => {
      this.categories = response;
    });
    this.employeeService.findAll().subscribe(response => {
      this.employees = response;
    });
  }

  onClick(): void {
    const formValue = this.form.value;

    const body: CalculationRqDto = {
      type: formValue.type,
      companies: formValue.companies,
      categories: formValue.categories,
      employees: formValue.employees,
      startDate: formValue.startDate,
      endDate: formValue.endDate
    }

    this.graphService.calculate(body).subscribe(response => {
      this.data = response;

      const data = {
        labels: this.data.map(row => row.label),
        datasets: [
          {
            label: this.form.value.type,
            data: this.data.map(row => row.data),
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
          }
        ]
      };

      this.buildChart(data);
    });
  }

  onDownload() {
    const timestamp = new Date().toISOString().replace(/T|\..+Z/, '').replace(/-/g, '');
    const base64string = this.chart!.toBase64Image();
    const url = window.URL.createObjectURL(this.base64ToBlob(base64string));
    const a = document.createElement('a');
    a.href = url;
    a.download = `chart-${timestamp}.png`;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }

  onSave() {
    const image = this.base64ToBlob(this.chart!.toBase64Image());
    const formData = new FormData();
    const formValue = this.form.value;

    formData.append('file', image);

    this.fileService.uploadFile(formData).pipe(concatMap((objectId: string) => {
      const body: ChartDto = {
        startPeriod: formValue.startDate,
        endPeriod: formValue.endDate,
        creationDate: moment().format('YYYY-MM-DDTHH:mm:ss'),
        name: formValue.type,
        type: 'График',
        objectId: objectId
      };
      return this.chartService.createChart(body)
    })).subscribe(_ => {
      console.log('Chart created!');
    }, error => {
      this.alertService.showAlert(error.error.status, error.error.message);
    });
  }

  private buildChart(data: any) {
    const canvas = <HTMLCanvasElement>document.getElementById('chart');
    this.chart?.destroy();
    this.chart = new Chart(canvas, {
      type: 'line',
      data: data,
      options: {
        plugins: {
          customCanvasBackgroundColor: {
            color: '#fff',
          }
        }
      },
      plugins: [plugin]
    });
  }

  private base64ToBlob(base64Image: string): Blob {
    const contentType = base64Image.split(',')[0].split(':')[1];
    const bytes = atob(base64Image.split(',')[1]);
    const buffer = new ArrayBuffer(bytes.length);
    const array = new Uint8Array(buffer);
    for (let i = 0; i < bytes.length; i++) {
      array[i] = bytes.charCodeAt(i);
    }
    return new Blob([buffer], { type: contentType });
  }
}
