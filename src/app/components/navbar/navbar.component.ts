import { Component } from '@angular/core';
import { UserDto } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  assets: string = '../../../assets';
  currentUser: UserDto | undefined;

  constructor(private userService: UserService) {
    this.currentUser = this.userService.getUserDetails();
  }
}
