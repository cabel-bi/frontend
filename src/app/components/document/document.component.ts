import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ArtifactDto } from 'src/app/models/artifact';
import { AlertService } from 'src/app/services/alert.service';
import { ArtifactService } from 'src/app/services/artifact.service';
import { ChartService } from 'src/app/services/chart.service';
import { FileService } from 'src/app/services/file.service';
import { DeleteComponent } from '../delete/delete.component';
import { concatMap } from 'rxjs';
import { ReportService } from 'src/app/services/report.service';
import { DiagramService } from 'src/app/services/diagram.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {

  title = 'Документы';
  artifacts: ArtifactDto[] = [];
  columns: string[] = ['name', 'creationDate', 'type', 'author', 'actions'];

  constructor(private artifactService: ArtifactService,
    private chartService: ChartService,
    private reportService: ReportService,
    private diagramService: DiagramService,
    private alertService: AlertService,
    private fileService: FileService,
    private dialog: MatDialog) {}

  ngOnInit(): void {
    this.artifactService.findArtifacts().subscribe(response => {
      this.artifacts = response;
    })
  }

  onDelete(id: number, objectId: string, type: string) {
    let deleteFunction: any;

    switch(type) {
      case 'График':
          deleteFunction = (args: {id: number, objectId: string, type: string}) => this.fileService.deleteFile(args.objectId).pipe(concatMap(() => this.chartService.deleteChart(args.id)));
          break;
        case 'Отчет':
          deleteFunction = (args: {id: number, objectId: string, type: string}) => this.fileService.deleteFile(args.objectId).pipe(concatMap(() => this.reportService.deleteReport(args.id)));
          break;
        case 'Диаграмма':
          deleteFunction = (args: {id: number, objectId: string, type: string}) => this.fileService.deleteFile(args.objectId).pipe(concatMap(() => this.diagramService.deleteDiagram(args.id)));
          break;
        default:
          deleteFunction = null;
          break;
    };

    const dialogRef = this.dialog.open(DeleteComponent, {
      data: {
        title: 'Удаление документа',
        message: 'Вы действительно хотите удалить документ?',
        deleteFunction: deleteFunction,
        args: {
          id: id,
          objectId: objectId,
          type: type
        }
      }
    });

    dialogRef.backdropClick().subscribe(_ => {
      dialogRef.close();
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response.id!) {
        this.artifacts = this.artifacts.filter(artifact => artifact.id !== response.id || artifact.type !== response.type);
      }
    })
  }

  onDownload(name: string, objectId: string) {
    this.fileService.getFile(objectId).subscribe(response => {
      const url = window.URL.createObjectURL(response.body!);
      const contentType = response.headers.get('Content-Type');
      const a = document.createElement('a');
      a.href = url;
      let extension = '';
      switch (contentType) {
        case 'application/pdf':
          extension = 'pdf';
          break;
        case 'image/png':
          extension = 'png';
          break;
        default:
          extension = '';
          break;
      }
      a.download = `${name}-${objectId}.${extension}`;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, error => {
      this.alertService.showAlert(error.error.status, error.error.message);
    });
  }
}
