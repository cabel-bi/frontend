import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import * as moment from 'moment';
import { CategoryService } from 'src/app/services/category.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FileService } from 'src/app/services/file.service';
import { AlertService } from 'src/app/services/alert.service';
import { concatMap } from 'rxjs';
import { Chart, ChartType } from 'chart.js/auto';
import plugin from 'src/app/plugins/plugin';
import { Company } from 'src/app/models/company';
import { UserService } from 'src/app/services/user.service';
import { UserDto } from 'src/app/models/user';
import { CompanyService } from 'src/app/services/company.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { Employee } from 'src/app/models/employee';
import { CalculationRqDto } from 'src/app/models/calculation-rq-dto';
import { CalculationService } from 'src/app/services/calculation.service';
import { CalculationRsDto } from 'src/app/models/calculation-rs-dto';
import { DiagramService } from 'src/app/services/diagram.service';
import { DiagramDto } from 'src/app/models/diagram';
import { color } from 'html2canvas/dist/types/css/types/color';

@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.css']
})
export class DiagramComponent implements OnInit {

  public title: String = 'Диаграммы';
  types: string[] = [
    'Лучшие товары',
    'Актуальные клиенты',
    'Продуктивные менеджеры',
    'Доли товаров',
    'Количество сделок по товарам'
  ];
  form: FormGroup;
  chart?: Chart;
  user: UserDto;
  data: CalculationRsDto[] = [];
  
  categories: Category[] = [];
  companies: Company[] = [];
  employees: Employee[] = [];

  constructor(
    private companyService: CompanyService,
    private categoryService: CategoryService,
    private employeeService: EmployeeService,
    private calculationService: CalculationService,
    private diagramService: DiagramService,
    private fileService: FileService,
    private alertService: AlertService,
    private userService: UserService) {
      this.form = new FormGroup({
        type: new FormControl('', Validators.required),
        companies: new FormControl<Company[]>([]),
        categories: new FormControl<Category[]>([]),
        employees: new FormControl<Employee[]>([]),
        startDate: new FormControl('', Validators.required),
        endDate: new FormControl('', Validators.required)
      });
      this.user = this.userService.getUserDetails()!;
    }

  ngOnInit(): void {
    this.companyService.findAll().subscribe(response => {
      this.companies = response;
    });
    this.categoryService.findAll().subscribe(response => {
      this.categories = response;
    });
    this.employeeService.findAll().subscribe(response => {
      this.employees = response;
    });
  }

  onClick(): void {
    const formValue = this.form.value;

    const body: CalculationRqDto = {
      type: formValue.type,
      companies: formValue.companies,
      categories: formValue.categories,
      employees: formValue.employees,
      startDate: formValue.startDate,
      endDate: formValue.endDate
    }

    this.calculationService.calculate(body).subscribe(response => {
      this.data = response;

      const length = this.data.length;

      const colors = Array.from({ length }).map(() => this.randomHexColor());

      console.log(colors);

      const data = {
        labels: this.data.map(row => row.label),
        datasets: [
          {
            label: this.form.value.type,
            data: this.data.map(row => row.data),
            backgroundColor: colors,
          }
        ]
      };

      switch(formValue.type) {
        case 'Доли товаров':
          this.buildPie(data);
          break;
        default:
          this.buildBar(data);
          break;
      };
    });
  }

  private randomHexColor(): string {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  onDownload() {
    const timestamp = new Date().toISOString().replace(/T|\..+Z/, '').replace(/-/g, '');
    const base64string = this.chart!.toBase64Image();
    const url = window.URL.createObjectURL(this.base64ToBlob(base64string));
    const a = document.createElement('a');
    a.href = url;
    a.download = `diagram-${timestamp}.png`;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }

  onSave() {
    const image = this.base64ToBlob(this.chart!.toBase64Image());
    const formData = new FormData();
    const formValue = this.form.value;

    formData.append('file', image);

    this.fileService.uploadFile(formData).pipe(concatMap((objectId: string) => {
      const body: DiagramDto = {
        name: formValue.name,
        type: 'Диаграмма',
        creationDate: moment().format('YYYY-MM-DDTHH:mm:ss'),
        objectId: objectId
      };
      return this.diagramService.createDiagram(body)
    })).subscribe(_ => {
      console.log('Diagram created!');
    }, error => {
      this.alertService.showAlert(error.error.status, error.error.message);
    });
  }

  private buildBar(data: any) {
    const canvas = <HTMLCanvasElement>document.getElementById('chart');
    this.chart?.destroy();
    this.chart = new Chart(canvas, {
      type: 'bar' as ChartType,
      data: data,
      options: {
        plugins: {
          customCanvasBackgroundColor: {
            color: '#fff',
          }
        }
      },
      plugins: [plugin]
    });
  }

  private buildPie(data: any) {
    const canvas = <HTMLCanvasElement>document.getElementById('chart');
    this.chart?.destroy();
    this.chart = new Chart(canvas, {
      type: 'pie' as ChartType,
      data: data,
      options: {
        plugins: {
          customCanvasBackgroundColor: {
            color: '#fff',
          },
          legend: {
            display: true,
            position: 'bottom'
          }
        }
      },
      plugins: [plugin]
    });
  }

  private base64ToBlob(base64Image: string): Blob {
    const contentType = base64Image.split(',')[0].split(':')[1];
    const bytes = atob(base64Image.split(',')[1]);
    const buffer = new ArrayBuffer(bytes.length);
    const array = new Uint8Array(buffer);
    for (let i = 0; i < bytes.length; i++) {
      array[i] = bytes.charCodeAt(i);
    }
    return new Blob([buffer], { type: contentType });
  }
}
