import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TokenService } from 'src/app/services/token.service';
import { AlertComponent } from '../alert/alert.component';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public email: string = '';
  public password: string = '';
  public rememberMe: boolean = false;

  @ViewChild('image') image!: ElementRef<HTMLImageElement>;
  @ViewChild('tooltip') tooltip!: ElementRef<HTMLDivElement>;

  constructor(private authService: AuthService,
    private tokenService: TokenService,
    private router: Router,
    private alertService: AlertService,
    private userService: UserService,
    private dialog: MatDialog) {}

  onSubmit() {
    this.authService.login(this.email, this.password).subscribe(response => {
      this.tokenService.setToken(response.accessToken, this.rememberMe);
      setTimeout(() => {
        this.userService.deleteUserDetails();
        this.tokenService.removeToken();
        this.router.navigate(['/auth']);
      }, 1200000);
      const email = this.tokenService.getSubject();
      this.userService.findByEmail(email).subscribe(response => {
        this.userService.setUserDetails(response);
        this.router.navigate(['/graphs']);
      }, error => {
        const dialogRef = this.dialog.open(AlertComponent);
  
        const instance = dialogRef.componentInstance;
  
        instance.status = error.error.status;
        instance.message = error.error.message;
  
        dialogRef.afterOpened().subscribe(_ => {
          setTimeout(() => dialogRef.close(), 2000);
        })
      });
    }, error => {
      if (typeof error.error === 'object' && 'status' in error.error) {
        this.alertService.showAlert(error.error.status, error.error.message);
      } else {
        alert(`${error.message}`);
      }
    });
    this.email = '';
    this.password = '';
  }

  showTooltip() {
    this.tooltip.nativeElement.style.display = 'flex';
  }

  hideTooltip() {
    this.tooltip.nativeElement.style.display = 'none';
  }
}
