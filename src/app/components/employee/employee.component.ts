import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserDto } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { RegistrationComponent } from '../registration/registration.component';
import { AlertService } from 'src/app/services/alert.service';
import { DeleteComponent } from '../delete/delete.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  public currentUser: UserDto | undefined;
  public title: string = 'Сотрудники';
  public users: UserDto[] = [];
  public columns: string[] = ['Имя, Фамилия', 'Должность', 'Электронная почта', 'Роль', 'actions'];

  constructor(private userService: UserService, private dialog: MatDialog, private alertService: AlertService) {}

  ngOnInit(): void {
    this.currentUser = this.userService.getUserDetails();
    this.userService.findAll().subscribe(response => {
      this.users = response;
    })
  }

  onDelete(id: number) {
    const dialogRef = this.dialog.open(DeleteComponent, {
      data: {
        title: 'Удаление сотрудника',
        message: 'Вы действительно хотите удалить сотрудника?',
        deleteFunction: (args: {id: number}) => this.userService.deleteUser(args.id),
        args: {
          id: id
        }
      }
    });

    dialogRef.backdropClick().subscribe(_ => {
      dialogRef.close();
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response.id!) {
        this.users = this.users.filter(user => user.id !== response.id);
      }
    })
  }

  onClick() {
    const dialogRef = this.dialog.open(RegistrationComponent);

    dialogRef.backdropClick().subscribe(_ => {
      dialogRef.close([]);
    })

    dialogRef.afterClosed().subscribe(user => {
      this.users = this.users.concat(user);
    })
  }
}
