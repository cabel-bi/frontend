import { Component } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserDto } from 'src/app/models/user';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  user: UserDto;
  isShowDropdown: boolean = false;

  constructor(private userService: UserService, private tokenService: TokenService) {
    this.user = this.userService.getUserDetails()!;
  }

  showDropdown() {
    this.isShowDropdown = true;
  }

  hideDropdown() {
    this.isShowDropdown = false;
  }

  onLogout() {
    this.userService.deleteUserDetails();
    this.tokenService.removeToken();
  }
}
