import { Component, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent {
  title: string;
  message: string;
  deleteFunction: (args: { [key: string]: any }) => Observable<void>;
  args: { [key: string]: any };

  constructor(private dialogRef: MatDialogRef<DeleteComponent>,
              private alertService: AlertService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
                this.title = data.title;
                this.message = data.message;
                this.deleteFunction = data.deleteFunction;
                this.args = data.args;
              }

  onClose() {
    this.dialogRef.close();
  }

  onDelete() {
    this.deleteFunction.call(this, this.args).subscribe(_ => {
      this.dialogRef.close({
        id: this.args['id'],
        type: this.args['type']
      });
    }, error => {
      this.alertService.showAlert(error.error.status, error.error.message);
      this.dialogRef.close(undefined);
    });
  }

}
