import { Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { ReportsComponent } from "./components/reports/reports.component";
import { GraphsComponent } from "./components/graphs/graphs.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { LoginComponent } from "./components/login/login.component";
import { DiagramComponent } from "./components/diagram/diagram.component";
import { DocumentComponent } from "./components/document/document.component";
import { EmployeeComponent } from "./components/employee/employee.component";
import { authGuard } from "./guards/auth.guard";


export const routes: Routes = [
  { path: 'auth', component: LoginComponent },
  { path: '', component: HomeComponent, canActivate: [authGuard]},
  { path: 'reports', component: ReportsComponent, canActivate: [authGuard]},
  { path: 'diagrams', component: DiagramComponent, canActivate: [authGuard] },
  { path: 'graphs', component: GraphsComponent, canActivate: [authGuard]},
  { path: 'docs', component: DocumentComponent, canActivate: [authGuard]},
  { path: 'employees', component: EmployeeComponent, canActivate: [authGuard]},
  { path: 'settings', component: SettingsComponent, canActivate: [authGuard]},
];