import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReportDto } from '../models/report';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private baseUrl: string = 'http://localhost:8080/reports';

  constructor(private http: HttpClient) { }

  public createReport(body: ReportDto): Observable<void> {
    return this.http.post<void>(this.baseUrl, body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }

  public deleteReport(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }
}
