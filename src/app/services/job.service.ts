import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JobDto } from '../models/job';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  private baseUrl: string = 'http://localhost:8080/jobs';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<JobDto[]> {
    return this.http.get<JobDto[]>(this.baseUrl);
  }
}
