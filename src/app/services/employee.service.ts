import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl: string = 'http://localhost:8080/employees';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.baseUrl);
  }
}
