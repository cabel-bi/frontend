import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDto } from '../models/user';
import { RegistrationRequest } from '../models/reg-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = 'http://localhost:8080/users';
  private userDetails: UserDto | undefined;

  constructor(private http: HttpClient) { }

  findByEmail(email: string): Observable<UserDto> {
    return this.http.get<UserDto>(`${this.baseUrl}/search?email=${email}`);
  }

  findAll(): Observable<UserDto[]> {
    return this.http.get<UserDto[]>(this.baseUrl);
  }

  createUser(body: RegistrationRequest): Observable<UserDto> {
    return this.http.post<UserDto>(this.baseUrl, body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    });
  }

  deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }

  getUserDetails() {
    if (!this.userDetails) {
      this.userDetails = JSON.parse(localStorage.getItem('user')!);
    }
    return this.userDetails;
  }

  setUserDetails(userDto: UserDto) {
    this.userDetails = userDto;
    localStorage.setItem('user', JSON.stringify(userDto));
  }

  deleteUserDetails() {
    if (this.userDetails) {
      this.userDetails = undefined;
      localStorage.removeItem('user');
    }
  }

}
