import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Company } from '../models/company';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private baseUrl: string = 'http://localhost:8080/companies';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Company[]> {
    return this.http.get<Company[]>(this.baseUrl);
  }
}
