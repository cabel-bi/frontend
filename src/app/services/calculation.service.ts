import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CalculationRqDto } from '../models/calculation-rq-dto';
import { Observable } from 'rxjs';
import { CalculationRsDto } from '../models/calculation-rs-dto';

@Injectable({
  providedIn: 'root'
})
export class CalculationService {

  private baseUrl: string = 'http://localhost:8080/calculations';

  constructor(private http: HttpClient) { }

  public calculate(body: CalculationRqDto): Observable<CalculationRsDto[]> {
    return this.http.post<CalculationRsDto[]>(this.baseUrl, body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }
}
