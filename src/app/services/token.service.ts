import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode'

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private key: string = "access_token";

  constructor() { }

  setToken(token: string, rememberMe: boolean) {
    if (rememberMe) {
      localStorage.setItem(this.key, token);
    }
    else {
      sessionStorage.setItem(this.key, token);
    }
  }

  getToken(): string | null {
    return localStorage.getItem(this.key) || sessionStorage.getItem(this.key);
  }

  removeToken() {
    sessionStorage.removeItem(this.key);
    localStorage.removeItem(this.key);
  }

  validateToken(): boolean {
    const token: string | null = this.getToken();

    try {
      const decoded = jwtDecode(token!);
      const exp = decoded.exp;

      if (exp && exp * 1000 < Date.now()) {
        return false;
      }
      return true;

    } catch (error) {
      return false;
    }
  }

  getSubject(): string {
    const token = this.getToken();
    const decoded = jwtDecode(token!);
    return decoded.sub!;
  }
}
