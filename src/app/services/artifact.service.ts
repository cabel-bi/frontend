import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ArtifactDto } from '../models/artifact';

@Injectable({
  providedIn: 'root'
})
export class ArtifactService {

  private baseUrl: string = 'http://localhost:8080/artifacts';

  constructor(private http: HttpClient) { }

  public findArtifacts(): Observable<ArtifactDto[]> {
    return this.http.get<ArtifactDto[]>(this.baseUrl);
  }
}
