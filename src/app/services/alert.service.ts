import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from '../components/alert/alert.component';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private dialog: MatDialog) { }

  public showAlert(status: string, message: string): void {
    const dialogRef = this.dialog.open(AlertComponent);

    dialogRef.afterOpened().subscribe(_ => {
      setTimeout(() => dialogRef.close(), 2000);
    })

    const instance = dialogRef.componentInstance;

    instance.status = status;
    instance.message = message;
  }

}
