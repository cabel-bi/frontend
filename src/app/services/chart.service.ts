import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChartDto } from '../models/chart';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  private baseUrl: string = 'http://localhost:8080/charts';

  constructor(private http: HttpClient) { }

  public createChart(body: ChartDto): Observable<void> {
    return this.http.post<void>(this.baseUrl, body, {
      headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
    });
  }

  public deleteChart(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }
}
