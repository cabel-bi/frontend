import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TableRowDto } from '../models/table-row';

@Injectable({
  providedIn: 'root'
})
export class AnalysisService {

  private baseUrl: string = 'http://localhost:8080/analysis';

  constructor(private http: HttpClient) { }

  public performAnalysis(body: any): Observable<TableRowDto[]> {
    return this.http.post<TableRowDto[]>(this.baseUrl, body, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'})
    });
  }
}
