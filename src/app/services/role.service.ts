import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RoleDto } from '../models/role';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private baseUrl: string = 'http://localhost:8080/roles';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<RoleDto[]> {
    return this.http.get<RoleDto[]>(this.baseUrl);
  }
}
