import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DiagramDto } from '../models/diagram';

@Injectable({
  providedIn: 'root'
})
export class DiagramService {

  private baseUrl: string = 'http://localhost:8080/diagrams';

  constructor(private http: HttpClient) { }

  public createDiagram(body: DiagramDto): Observable<void> {
    return this.http.post<void>(this.baseUrl, body, {
      headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
    });
  }

  public deleteDiagram(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }
}
