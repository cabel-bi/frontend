import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private baseUrl: string = 'http://localhost:8080/files';

  constructor(private http: HttpClient) { }

  public uploadFile(formData: FormData): Observable<string> {
    return this.http.post<string>(this.baseUrl, formData, {
      responseType: 'text' as 'json'
    });
  }

  public getFile(id: string): Observable<HttpResponse<Blob>> {
    return this.http.get<Blob>(`${this.baseUrl}/${id}`, {
      observe: 'response',
      responseType: 'blob' as 'json'
    });
  }

  public deleteFile(id: string): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  } 
}
