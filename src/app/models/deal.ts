export interface Deal {
    id: number;
    status: string;
    creationDate: string;
    closingDate: string;
    productIds: number[];
}