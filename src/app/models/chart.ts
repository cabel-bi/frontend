export interface ChartDto {
    startPeriod: string;
    endPeriod: string; 
    creationDate: string;
    name: string;
    type: string;
    objectId: string;
}