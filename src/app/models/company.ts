export interface Company {
    id: number,
    name: string,
    country: string,
    region: string,
    district: string,
    city: string
}