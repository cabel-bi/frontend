export interface ErrorDto {
    status: string,
    message: string
}