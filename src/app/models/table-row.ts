export interface TableRowDto {
    title: string,
    sales: number,
    share: number,
    cumulativeShare: number,
    abc: string,
    xyz: string
}