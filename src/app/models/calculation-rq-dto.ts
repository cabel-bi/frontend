import { Category } from "./category";
import { Company } from "./company";
import { Employee } from "./employee";

export interface CalculationRqDto {
    type: string,
    companies: Company[],
    categories: Category[],
    employees: Employee[],
    startDate: string,
    endDate: string
}