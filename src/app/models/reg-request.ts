import { JobDto } from "./job";
import { RoleDto } from "./role";

export interface RegistrationRequest {
    firstName: string,
    lastName: string,
    email: string,
    job: JobDto,
    roles: RoleDto[]
}