import { UserDto } from "./user";

export interface ArtifactDto {
    id: number;
    type: string;
    objectId: string;
    name: string;
    creationDate: string;
    userDto: UserDto;
}