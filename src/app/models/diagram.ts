export interface DiagramDto {
    name: string,
    type: string,
    creationDate: string,
    objectId: string
}