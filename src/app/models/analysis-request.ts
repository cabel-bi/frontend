export interface AnalysisRequestDto {
    type: string,
    startDate: string,
    endDate: string
}