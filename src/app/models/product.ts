export interface Product {
    id: number;
    categoryId: number;
    title: string;
    price: number;
}