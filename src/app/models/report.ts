export interface ReportDto {
    name: string,
    description: string | null,
    creationDate: string,
    type: string,
    objectId: string
}