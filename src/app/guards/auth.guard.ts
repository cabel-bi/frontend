import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { TokenService } from '../services/token.service';

export const authGuard: CanActivateFn = (route, state) => {
  const router: Router = inject(Router);
  const tokenService: TokenService = inject(TokenService);

  if (tokenService.validateToken())
    return true;

  router.navigate(['/auth']);
  return false;
};
